#!/usr/bin/env node
"use strict";
const green = require("chalk").green;
const inquirer = require("inquirer");
const utils = require("./utils");
const apiClient = require("./apiClient")
const Mixer = require("./mixer")
const mixer = new Mixer()

apiClient.getHouseBalace().then((response) => {
  mixer.setHouseBalance(response.data.balance)
  console.log("Welcome to the Jobcoin mixer!");
  prompt()
})

function prompt() {
  /* Inquirer documentation: https://github.com/SBoudrias/Inquirer.js#documentation */
  const deposit = utils.generateDepositAddress()

  inquirer.prompt([
    {
      name: "addresses",
      message: "Please enter a comma-separated list of new, unused Jobcoin addresses where your mixed Jobcoins will be sent:"
    },
    {
      name: "deposit",
      message: `You may now send Jobcoins to address ${green(deposit)}. They will be mixed and sent to your destination addresses. \n Enter ${green('"y"')} to run again.`,
      when: (answers) => answers.addresses
    },
  ])
  .then((answers) => {
    mixer.addUserWalletInfo(deposit, answers.addresses)
    apiClient.depositPoller(deposit, mixer.getWallet(deposit), mixer);
    if (answers.deposit && answers.deposit.toLowerCase() === "y") {
      prompt();
    }
  });
}





