#!/usr/bin/env node
"use strict";
const axios = require("axios");

/* Urls */
const House = 'House';
const API_BASE_URL = "http://jobcoin.gemini.com/unrelated/api";
const API_TRANSACTIONS_URL = `${API_BASE_URL}/transactions`;
const API_GET_HOUSE_BALANCE = `${API_BASE_URL}/addresses/${House}`;
let timeoutid

function walletTransactions () {
    return axios.get(API_TRANSACTIONS_URL)
}

function sendCoins({from, to, amt}) {
    return axios.post(API_TRANSACTIONS_URL, {
        fromAddress: from,
        toAddress: to,
        amount: amt
    })
}

function getHouseBalace() {
    return axios.get(`${API_GET_HOUSE_BALANCE}`)
}

function distributeCoins (wallet) {
    const addresses = wallet.userMixerAddressess.split(',')
    const despositPieces = wallet.depositBalance / addresses.length;
    const depositPromises = []

   for (let i = 0; i < addresses.length; i++) {
       depositPromises.push(sendCoins({from: House, to: addresses[i], amt: despositPieces}))
   }

   return Promise.all(depositPromises)
}

function depositPoller (deposit, wallet, callback) {
    walletTransactions().then(function(response){
        let depositRecieved = lookForDeposits(response.data, deposit)
        if (depositRecieved) {
            console.log("Deposit found")
            callback.depositRecieved(wallet, depositRecieved)
            clearTimeout(timeoutid)
        } else {
            console.log("Deposit not found")
            timeoutid = setTimeout(() => depositPoller(deposit, wallet, callback), 600)
        }
    })
}

function lookForDeposits(data, deposit) {
    for (let i = 0; i < data.length; i++) {
        if (data[i].toAddress === deposit) {
            return data[i]
        }
    }
    return null
}

exports.depositPoller  = depositPoller;
exports.sendCoins = sendCoins;
exports.getHouseBalace = getHouseBalace;
exports.distributeCoins = distributeCoins;
exports.House = 'House';