#!/usr/bin/env node
"use strict";
const apiClient = require("./apiClient")

class Mixer {
    constructor() {
        this.wallets = {}
        this.houseBalance = 0
    }

    setHouseBalance(balance) {
        this.houseBalance = balance
    }

    addUserWalletInfo(deposit, userMixerAddressess) {
        this.wallets[deposit] = {
            depositAddress: deposit,
            userMixerAddressess: userMixerAddressess,
            depositBalance: 0,
            mixedWithHouse: false
        }
    }

    getWallet(deposit) {
        return this.wallets[deposit]
    }

    depositRecieved(wallet, depositRecieved) {
        const mixer = this;
        const currentWallet = mixer.wallets[wallet.depositAddress]
        currentWallet.depositBalance = depositRecieved.amount
        apiClient.sendCoins({from: currentWallet.depositAddress, to: apiClient.House, amt: currentWallet.depositBalance}).then((data) => {
            currentWallet.mixedWithHouse = true;
            mixer.releaseUserFunds(wallet)
            return apiClient.getHouseBalace()
        }).then((response) => {
            mixer.houseBalance = response.data.balance;
        })
    }

    releaseUserFunds(wallet){
        const mixer = this
        apiClient.distributeCoins(wallet).then((response) => {            
            mixer.afterRelease(wallet)
            return apiClient.getHouseBalace()
        }).then((response) => {
            console.log(response.data.balance)
        })
    }

    afterRelease(wallet) {
        delete this.wallets[wallet.deposit]
    }

  }

  module.exports = Mixer